package com.task.dto;

import lombok.Data;

import java.util.List;

@Data

public class RegionalBlocsDto {
    String acronym;
    String name;
    List<String> otherAcronyms;
    List<String> otherNames;

    @Override
    public String toString(){
        return "("+"acronym: "+acronym+", name: "+name+", otherAcronyms: "+otherAcronyms+", otherNames: "+otherNames+")";
    }
}
