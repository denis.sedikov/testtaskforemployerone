package com.task.dto;

import lombok.Data;

@Data
public class CurrenciesDto {
    String code;
    String name;
    String symbol;
    @Override
    public String toString() {
        return "("+"Code: "+ code + ", Name: " + name +", Symbol: "+symbol+")";
    }
}
