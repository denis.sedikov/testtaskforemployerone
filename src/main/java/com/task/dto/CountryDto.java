package com.task.dto;


import lombok.Data;
import java.util.List;

@Data
public class CountryDto {
    String name;
    List<String> topLevelDomain;
    String population;
    List<String> latlng;
    List <CurrenciesDto> currencies;
    List <LanguagesDto> languages;
    TranslationsDto translations;
    String flag;
    List <RegionalBlocsDto> regionalBlocs;
}
