package com.task.dto;

import lombok.Data;


@Data
public class LanguagesDto {
    String iso639_1;
    String iso639_2;
    String name;
    String nativeName;
    @Override
    public String toString() {
        return "("+"iso639_1: "+ iso639_1+ ", iso639_2: " + iso639_2 +", name: "+name+", nativeName: "+nativeName+")";
    }
}
