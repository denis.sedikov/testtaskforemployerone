package com.task.service.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.task.dto.CountryDto;
import com.task.model.Country;
import com.task.repository.CountryRepos;
import com.task.service.Initialize;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class InitializeImpl implements Initialize {

    private final CountryRepos countryRepos;

    @Autowired
    public InitializeImpl(CountryRepos countryRepos){
        this.countryRepos = countryRepos;
        saveData(getData());
    }

    @Override
    public List<CountryDto> getData() {
        String url = "https://restcountries.eu/rest/v2/all";
        List<CountryDto> listItemsDes = null;
        try {
            HttpResponse response = Request.Get(url).execute().returnResponse();
            try {
                JSONArray jsonResponse = new JSONArray(IOUtils.toString(response.getEntity().getContent()));
                Type itemsListType = new TypeToken<List<CountryDto>>() {}.getType();
                listItemsDes = new Gson().fromJson(String.valueOf(jsonResponse),itemsListType);
                return listItemsDes;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listItemsDes;
    }

    @Override
    public void saveData(List<CountryDto> list) {
            if (list!=null){
                for(CountryDto countryDtoTemp : list) {
                    countryRepos.save(countryDtoToCountry(countryDtoTemp));
                }

            }
    }

    private Country countryDtoToCountry(CountryDto countryDto){

        Country country = new Country();

        country.setCurrencies(countryDto.getCurrencies().toString());
        country.setFlag(countryDto.getFlag());
        country.setLanguages(countryDto.getLanguages().toString());
        country.setLatlng(countryDto.getLatlng().toString());
        country.setName(countryDto.getName());
        country.setPopulation(countryDto.getPopulation());
        country.setRegionalBlocs(countryDto.getRegionalBlocs().toString());
        country.setTopLevelDomain(countryDto.getTopLevelDomain().toString());
        country.setTranslations(countryDto.getTranslations().toString());

        return country;
    }
}
