package com.task.controller;

import com.task.model.Country;
import com.task.repository.CountryRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/data")
public class ResponseController {

    CountryRepos countryRepos;

    @Autowired
    public ResponseController(CountryRepos countryRepos){
     this.countryRepos = countryRepos;
    }


    @GetMapping("/domain")
    public ResponseEntity getByDomain(@RequestParam String domain) {
        if (domain != null) {
            List<Country> countries = countryRepos.findByContainsDomain(domain).orElse(null);
            if (countries == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("country not found");
            }else {
                return ResponseEntity.ok(countries);
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("country not found");

    }

    @GetMapping("/name")
    public ResponseEntity getByName(@RequestParam String name) {
        if(name!=null){
            List<Country> countries = countryRepos.findByName(name).orElse(null);
            if (countries == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("country not found");
            }else {
                return ResponseEntity.ok(countries);
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("country not found");
    }
}
