package com.task.controller;

import com.task.model.Country;
import com.task.repository.CountryRepos;
import com.task.service.Initialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/update")
public class UpdateDBController {

    private  final CountryRepos countryRepos;
    private final Initialize initialize;

    @Autowired
    public UpdateDBController(CountryRepos countryRepos,Initialize initialize){
        this.countryRepos = countryRepos;
        this.initialize = initialize;
    }
    @GetMapping
    public ResponseEntity login(@RequestParam String cleanUpdate){

        if (cleanUpdate.equals("true")){
            List<Country> countries = countryRepos.findAll();
            for (Country tempCountry : countries){
                countryRepos.delete(tempCountry);
            }

            initialize.saveData(initialize.getData());

            return ResponseEntity.ok("database was create-droped");
        }else {
            return ResponseEntity.ok("value of cleanUpdate must be 'true' for clean and update db");
        }
    }
}
