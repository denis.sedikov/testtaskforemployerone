package com.task.repository;

import com.task.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CountryRepos extends JpaRepository<Country,Long> {
    @Query("from Country where topLevelDomain like %?1%")
    Optional<List<Country>> findByContainsDomain(String domain);
    @Query("from Country  where lower(name) = lower(?1) ")
    Optional<List<Country>> findByName(String name);
}
